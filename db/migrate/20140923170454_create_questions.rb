class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :ask
      t.boolean :status
      t.integer :lesson_id
      t.integer :student_id

      t.timestamps
    end
  end
end
