class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.string :name
      t.string :aula
      t.integer :teacher_id

      t.timestamps
    end

    create_table :lessons_students, id: false do |t|
      t.belongs_to :lesson
      t.belongs_to :student
    end
  end
end
