# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

teacher = Teacher.create(name: Faker::Name.name)
puts "profe: " << teacher.name

10.times do
	student = Student.create(name:Faker::Name.name)
	puts "alu: " << student.name
end

5.times do
	lesson = Lesson.new(name: Faker::Commerce.department(1), aula: Faker::Address.secondary_address,teacher_id: teacher.id)
	lesson.students << Student.all.shuffle[0,3]
	lesson.save
	puts "lesson: " << lesson.name	
end